SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `sklad` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `sklad` ;

-- -----------------------------------------------------
-- Table `sklad`.`goods`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sklad`.`goods` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(45) NOT NULL COMMENT 'Карточка товара' ,
  `text` TEXT NOT NULL COMMENT 'Описание товара' ,
  `date` TIMESTAMP NOT NULL DEFAULT now() COMMENT 'Дата производства' ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `title_UNIQUE` (`title` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `sklad`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sklad`.`users` (
  `login` VARCHAR(15) NOT NULL COMMENT 'Логин' ,
  `pass` VARCHAR(45) NOT NULL COMMENT 'Пароль' ,
  PRIMARY KEY (`login`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `sklad`.`groupuser`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sklad`.`groupuser` (
  `name` VARCHAR(20) NOT NULL COMMENT 'Наименование группы' ,
  `users_login` VARCHAR(15) NOT NULL COMMENT 'Вторичный ключ от таблицы users' ,
  PRIMARY KEY (`name`) ,
  INDEX `fk_groupuser_users` (`users_login` ASC) ,
  CONSTRAINT `fk_groupuser_users`
    FOREIGN KEY (`users_login` )
    REFERENCES `sklad`.`users` (`login` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `sklad`.`messages`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sklad`.`messages` (
  `id` INT NOT NULL ,
  `text` VARCHAR(255) NOT NULL COMMENT 'Текст сообщения' ,
  `date` TIMESTAMP NOT NULL DEFAULT now() COMMENT 'Дата мессаги' ,
  `users_login` VARCHAR(15) NOT NULL COMMENT 'Юзер пославший мессагу' ,
  `goods_id` INT NOT NULL COMMENT 'Товару к которому послан комент' ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_messages_users1` (`users_login` ASC) ,
  INDEX `fk_messages_goods1` (`goods_id` ASC) ,
  CONSTRAINT `fk_messages_users1`
    FOREIGN KEY (`users_login` )
    REFERENCES `sklad`.`users` (`login` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_goods1`
    FOREIGN KEY (`goods_id` )
    REFERENCES `sklad`.`goods` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `sklad`.`groupuser_has_goods`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sklad`.`groupuser_has_goods` (
  `groupuser_name` VARCHAR(20) NOT NULL ,
  `goods_id` INT NOT NULL ,
  PRIMARY KEY (`groupuser_name`, `goods_id`) ,
  INDEX `fk_groupuser_has_goods_goods1` (`goods_id` ASC) ,
  INDEX `fk_groupuser_has_goods_groupuser1` (`groupuser_name` ASC) ,
  CONSTRAINT `fk_groupuser_has_goods_groupuser1`
    FOREIGN KEY (`groupuser_name` )
    REFERENCES `sklad`.`groupuser` (`name` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_groupuser_has_goods_goods1`
    FOREIGN KEY (`goods_id` )
    REFERENCES `sklad`.`goods` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
