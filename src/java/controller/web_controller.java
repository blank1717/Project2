/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import java.io.IOException;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import session.GoodsFacade;
@WebServlet(name = "web_controller", loadOnStartup=1, urlPatterns = {"/goods", "/registration"})
public class web_controller extends HttpServlet {
    
    @EJB
    GoodsFacade goodsFacade;
    @Override
    public void init() throws ServletException {
        getServletContext().setAttribute("goods", goodsFacade.findAll());
    }
protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String userPath=request.getServletPath();
        if ("/goods".equals(userPath)){
            // TODO: обработка запроса статьи
        }else
        if ("/registration".equals(userPath)){
            //TODO: обработка запроса регистрации
        }
        request.getRequestDispatcher("/WEB-INF/views"+userPath+".jsp").forward(request, response);
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
        @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}