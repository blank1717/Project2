<%@page contentType="text/html" pageEncoding="UTF-8"%>

        <div id="main">
            <aside class="leftAside">
                <h2>Категории товаров</h2>
                <ul>
                    <li><a href="#">Вид товара 1</a></li>
                    <li><a href="#">Вид товара 2</a></li>
                    <li><a href="#">Вид товара 3</a></li>
                    <li><a href="#">Вид товара 4</a></li>
                </ul>
            </aside>
            <section>                                   
                    <c:forEach var="good" items="${goods}">
                        <article>
			<h1>${good.title}</h1>
                        <div class="text-article">
                        ${fn:substring(good.text,0,300)} ...
                        </div>
                        <div class="fotter-article">
                        <span class="read"><a href="good?id=${good.id}">Смотреть...</a></span>
                        <span class="date-article">Дата производства: ${good.date}</span>
                        </div>
                        </article>
		    </c:forEach>
            </section>
        </div>


