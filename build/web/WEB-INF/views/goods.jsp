<%@page contentType="text/html" pageEncoding="UTF-8"%>

        <div id="main">
            <aside class="leftAside">
                <h2>Категории товаров</h2>
                <ul>
                    <li><a href="#">Вид товара 1</a></li>
                    <li><a href="#">Вид товара 2</a></li>
                    <li><a href="#">Вид товара 3</a></li>
                    <li><a href="#">Вид товара 4</a></li>
                   
                </ul>
            </aside>
            <section>
                <article>
                    <h1>Мыло</h1>
                    <div class="text-article">
                        Мыло туалетное предназначается для ежедневного использования в санитарно-гигиенических целях. Мыло туалетное кусковое выпускается в виде прямоугольных брусков весом 100 г и упакованных в прозрачную обертку. Из-за оптимального соотношения цены и качества его можно отнести к сегменту Суперэконом. Мыло подходит тем, кто желает сэкономить.
В основу состава мыла входят натуральные животные жиры, натриевые соли жирных кислот кокосового и пальмового масел, вода, едкий натр, ароматические композиции и др. компоненты. Товар производится в соответствии с ГОСТ 28546 2002 и отвечает всем требованиям.
Основные характеристики кускового туалетного мыла
Для производства высококачественного мыла используются только натуральные компоненты, поэтому продукт имеет приятный запах, не содержит агрессивных химических соединений и обладает многими полезными свойствами:
 высокой моющей способностью; особыми гигиеническими свойствами; подходит для всех видов кожи, не сушит ее; обладает сильным антибактериальным эффектом;экономично в использовании.
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Производитель: <a href="#">Компания1</a></span>
                        <span class="date-article">Дата производства: 20.12.2012</span>
                    </div>
                </article>
            </section>
        </div>


