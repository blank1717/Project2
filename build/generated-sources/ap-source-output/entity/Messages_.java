package entity;

import entity.Goods;
import entity.Users;
import java.util.Date;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-01-28T21:15:19", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(Messages.class)
public class Messages_ { 

    public static volatile SingularAttribute<Messages, Date> date;
    public static volatile SingularAttribute<Messages, Goods> goodsId;
    public static volatile SingularAttribute<Messages, Users> usersLogin;
    public static volatile SingularAttribute<Messages, Integer> id;
    public static volatile SingularAttribute<Messages, String> text;

}