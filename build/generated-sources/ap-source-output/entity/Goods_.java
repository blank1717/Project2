package entity;

import entity.Groupuser;
import entity.Messages;
import java.util.Date;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-01-28T21:15:19", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(Goods.class)
public class Goods_ { 

    public static volatile SingularAttribute<Goods, Date> date;
    public static volatile CollectionAttribute<Goods, Groupuser> groupuserCollection;
    public static volatile SingularAttribute<Goods, Integer> id;
    public static volatile SingularAttribute<Goods, String> text;
    public static volatile SingularAttribute<Goods, String> title;
    public static volatile CollectionAttribute<Goods, Messages> messagesCollection;

}