package entity;

import entity.Goods;
import entity.Users;
import javax.annotation.processing.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="org.eclipse.persistence.internal.jpa.modelgen.CanonicalModelProcessor", date="2023-01-28T21:15:19", comments="EclipseLink-2.7.10.v20211216-rNA")
@StaticMetamodel(Groupuser.class)
public class Groupuser_ { 

    public static volatile SingularAttribute<Groupuser, String> name;
    public static volatile SingularAttribute<Groupuser, Users> usersLogin;
    public static volatile CollectionAttribute<Groupuser, Goods> goodsCollection;

}